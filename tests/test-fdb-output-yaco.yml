# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

test_collection_size:
  test2d: 1
  test3d: 40
fdb_writer:
  collections:
    test2d:
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 70
        - typeOfFirstFixedSurface: 1
        - typeOfSecondFixedSurface: 8
    test3d:
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 24
        - scaledValueOfFirstFixedSurface:
            source: yaco
            key: collection_index
            lookup: accumulate_half
            lookup_table:
              - 12
              - 10
              - 10
              - 10
              - 10
              - 10
              - 13
              - 15
              - 20
              - 25
              - 30
              - 35
              - 40
              - 45
              - 50
              - 55
              - 60
              - 70
              - 80
              - 90
              - 100
              - 110
              - 120
              - 130
              - 140
              - 150
              - 170
              - 180
              - 190
              - 200
              - 220
              - 250
              - 270
              - 300
              - 350
              - 400
              - 450
              - 500
              - 500
              - 600
        - vertical.typeOfLevel: depthBelowSea
        # - discipline: 0
        # - parameterCategory: 3
        # - parameterNumber: 4
        # - scaledValueOfFirstFixedSurface:
        #     source: yaco
        #     key: collection_index
        #     offset: 1
        # - scaledValueOfSecondFixedSurface:
        #     source: yaco
        #     key: collection_index
        #     offset: 2
        # - vertical.typeOfLevel: generalVertical
        # - nlev: 10
        # - numberOfVGridUsed: 1
        # - uuidOfVGrid:
        #     value: db3cbe11f0a3f32114b0f72e8d122b20
        #     convert: hex_to_bytes
  fdb_config: test-fdb-output-fdb.yml
  fdb_metadata:
    - class:
        source: grib
        key: class
    - expid:
        source: grib
        key: experiment
    - version:
        source: grib
        key: experimentVersionNumber
    - source:
        source: grib
        key: dataset
    - variable:
        source: grib
        key: paramId
    - levtype:
        source: grib
        key: mars.levtype
    - level:
        source: grib
        key: mars.levelist
        maybeempty: true
    - datetime:
        source: yaco
        key: timestep_begin
        format: "%Y%m%d %H%M%S"
    - grid:
        source: grid
        key: name
  grib_metadata:
    - centre: ecmf
    - subCentre: 1003
    - tablesVersion: 32
    - typeOfProcessedData: 1
    - setLocalDefinition: 1 # write section 2
    - significanceOfReferenceTime: 2
    - year:
        source: yaco
        key: timestep_begin
        format: "%Y"
    - month:
        source: yaco
        key: timestep_begin
        format: "%m"
    - day:
        source: yaco
        key: timestep_begin
        format: "%d"
    - hour:
        source: yaco
        key: timestep_begin
        format: "%H"
    - minute:
        source: yaco
        key: timestep_begin
        format: "%M"
    - second:
        source: yaco
        key: timestep_begin
        format: "%S"
    - productionStatusOfProcessedData: 12
    - productDefinitionTemplateNumber: 8 # statistics over time
    - typeOfStatisticalProcessing: avg
    - indicatorOfUnitForTimeRange: h
    - lengthOfTimeRange: 6
    - indicatorOfUnitForTimeIncrement: h
    - timeIncrement: 6
    - yearOfEndOfOverallTimeInterval:
        source: yaco
        key: timestep_end
        format: "%Y"
    - monthOfEndOfOverallTimeInterval:
        source: yaco
        key: timestep_end
        format: "%m"
    - dayOfEndOfOverallTimeInterval:
        source: yaco
        key: timestep_end
        format: "%d"
    - hourOfEndOfOverallTimeInterval:
        source: yaco
        key: timestep_end
        format: "%H"
    - minuteOfEndOfOverallTimeInterval:
        source: yaco
        key: timestep_end
        format: "%M"
    - secondOfEndOfOverallTimeInterval:
        source: yaco
        key: timestep_end
        format: "%S"
    - dataset: climate-dt
    - class: d1
    - activity: CMIP6 # TODO set from environment variable
    - experiment: hist # TODO set from environment variable
    - generation: 1 # TODO set from environment variable
    - mars.stream: clte
    - mars.type: fc
    - model: ICON
    - realization: 1 # TODO set from environment variable
    - resolution: standard # TODO set from environment variable
    - experimentVersionNumber: 1337 # TODO set from environment variable
    - gridType:
        source: grid
        key: type
    - Nside:
        source: grid
        key: sides
    - orderingConvention:
        source: grid
        key: order
grid:
  name: healpix_2
  nside: 5
  order: nested
  type: healpix
output: fdb
