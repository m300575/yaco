// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <stdexcept>
#include <yaco/chrono.hpp>

extern "C" {
#include <mtime_datetime.h>
#include <mtime_julianDay.h>
#include <mtime_timedelta.h>
}

namespace yaco::chrono
{

Duration::Duration() : impl_(nullptr)
{
    struct _timedelta* td = newTimeDelta("PT0S");
    if (td == NULL) {
        throw std::runtime_error("Invalid duration format");
    }
    impl_ = td;
}

Duration::Duration(const std::string& iso8601) : impl_(nullptr)
{
    struct _timedelta* td = newTimeDelta(iso8601.c_str());
    if (td == NULL) {
        throw std::runtime_error("Invalid duration format");
    }
    impl_ = td;
}

Duration::Duration(const Duration& other) : impl_(nullptr)
{
    struct _timedelta* td = constructAndCopyTimeDelta(static_cast<struct _timedelta*>(other.impl_));
    if (td == NULL) {
        throw std::runtime_error("Failed to copy Duration");
    }
    impl_ = td;
}

Duration& Duration::operator=(const Duration& other)
{
    if (this != &other) {
        struct _timedelta* td = constructAndCopyTimeDelta(static_cast<struct _timedelta*>(other.impl_));
        if (td == NULL) {
            throw std::runtime_error("Failed to copy Duration");
        }
        if (impl_)
            deallocateTimeDelta(static_cast<struct _timedelta*>(impl_));
        impl_ = td;
    }
    return *this;
}

Duration::Duration(Duration&& other) noexcept : impl_(other.impl_)
{
    other.impl_ = nullptr;
}

Duration& Duration::operator=(Duration&& other) noexcept
{
    if (this != &other) {
        if (impl_)
            deallocateTimeDelta(static_cast<struct _timedelta*>(impl_));
        impl_       = other.impl_;
        other.impl_ = nullptr;
    }
    return *this;
}

Duration::~Duration()
{
    if (impl_)
        deallocateTimeDelta(static_cast<struct _timedelta*>(impl_));
}

std::chrono::seconds Duration::toSecondsFrom(const DateTime& refTime) const noexcept
{
    struct _datetime dt = *static_cast<const struct _datetime*>(refTime.impl_);
    return std::chrono::seconds(getTotalSecondsTimeDelta(static_cast<struct _timedelta*>(impl_), &dt));
}

std::chrono::milliseconds Duration::toMillisecondsFrom(const DateTime& refTime) const noexcept
{
    struct _datetime dt = *static_cast<const struct _datetime*>(refTime.impl_);
    return std::chrono::milliseconds(getTotalMilliSecondsTimeDelta(static_cast<struct _timedelta*>(impl_), &dt));
}

std::chrono::seconds Duration::toSeconds() const
{
    struct _timedelta* td = static_cast<struct _timedelta*>(impl_);
    if (td->month != 0 || td->year != 0) {
        throw std::runtime_error("Duration is not a time duration");
    }
    return std::chrono::seconds(td->day * 86400 + td->hour * 3600 + td->minute * 60 + td->second);
}

std::chrono::milliseconds Duration::toMilliseconds() const noexcept
{
    std::chrono::seconds s = toSeconds();
    return std::chrono::milliseconds(s.count() * 1000 + static_cast<struct _timedelta*>(impl_)->ms);
}

Duration::ModuloResult Duration::modulo(const Duration& other) const noexcept
{
    ModuloResult result;
    result.remainder = std::chrono::milliseconds(moduloTimedelta(static_cast<struct _timedelta*>(impl_), static_cast<struct _timedelta*>(other.impl_), &result.quotient));
    return result;
}

bool Duration::operator==(const Duration& other) const noexcept
{
    return compareTimeDelta(static_cast<struct _timedelta*>(impl_), static_cast<struct _timedelta*>(other.impl_)) == equal_to;
}

bool Duration::operator!=(const Duration& other) const noexcept
{
    return !(*this == other);
}

bool Duration::operator<(const Duration& other) const noexcept
{
    return compareTimeDelta(static_cast<struct _timedelta*>(impl_), static_cast<struct _timedelta*>(other.impl_)) == less_than;
}

bool Duration::operator>(const Duration& other) const noexcept
{
    return compareTimeDelta(static_cast<struct _timedelta*>(impl_), static_cast<struct _timedelta*>(other.impl_)) == greater_than;
}

bool Duration::operator<=(const Duration& other) const noexcept
{
    return *this < other || *this == other;
}

bool Duration::operator>=(const Duration& other) const noexcept
{
    return *this > other || *this == other;
}

void Duration::invert() noexcept
{
    auto* td = static_cast<struct _timedelta*>(impl_);
    td->sign *= -1;
}

} // namespace yaco::chrono
