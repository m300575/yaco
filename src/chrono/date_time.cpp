// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <stdexcept>
#include <yaco/chrono.hpp>

extern "C" {
#include <mtime_datetime.h>
#include <mtime_julianDay.h>
#include <mtime_timedelta.h>
}

namespace yaco::chrono
{

DateTime::DateTime(const std::string& iso8601) : impl_(nullptr)
{
    struct _datetime* dt = newDateTime(iso8601.c_str());
    if (dt == NULL) {
        throw std::runtime_error("Invalid timestep format");
    }
    impl_ = dt;
}

DateTime::DateTime(const DateTime& other) : impl_(nullptr)
{
    struct _datetime* dt = constructAndCopyDateTime(static_cast<struct _datetime*>(other.impl_));
    if (dt == NULL) {
        throw std::runtime_error("Failed to copy DateTime");
    }
    impl_ = dt;
}

DateTime& DateTime::operator=(const DateTime& other)
{
    if (this != &other) {
        struct _datetime* dt = constructAndCopyDateTime(static_cast<struct _datetime*>(other.impl_));
        if (dt == NULL) {
            throw std::runtime_error("Failed to copy DateTime");
        }
        if (impl_)
            deallocateDateTime(static_cast<struct _datetime*>(impl_));
        impl_ = dt;
    }
    return *this;
}

DateTime::DateTime(DateTime&& other) noexcept : impl_(other.impl_)
{
    other.impl_ = nullptr;
}

DateTime& DateTime::operator=(DateTime&& other) noexcept
{
    if (this != &other) {
        if (impl_)
            deallocateDateTime(static_cast<struct _datetime*>(impl_));
        impl_       = other.impl_;
        other.impl_ = nullptr;
    }
    return *this;
}

DateTime::~DateTime()
{
    if (impl_)
        deallocateDateTime(static_cast<struct _datetime*>(impl_));
}

int DateTime::year() const noexcept
{
    return static_cast<int>(static_cast<struct _datetime*>(impl_)->date.year);
}

int DateTime::month() const noexcept
{
    return static_cast<struct _datetime*>(impl_)->date.month;
}

int DateTime::day() const noexcept
{
    return static_cast<struct _datetime*>(impl_)->date.day;
}

int DateTime::hour() const noexcept
{
    return static_cast<struct _datetime*>(impl_)->time.hour;
}

int DateTime::minute() const noexcept
{
    return static_cast<struct _datetime*>(impl_)->time.minute;
}

int DateTime::second() const noexcept
{
    return static_cast<struct _datetime*>(impl_)->time.second;
}

std::string DateTime::toString(const char* fmt) const noexcept
{
    char datetime_buffer[256];
    datetimeToPosixString(static_cast<struct _datetime*>(impl_), datetime_buffer, const_cast<char*>(fmt));
    return datetime_buffer;
}

bool DateTime::operator<(const DateTime& other) const noexcept
{
    return compareDatetime(static_cast<struct _datetime*>(impl_), static_cast<struct _datetime*>(other.impl_)) == less_than;
}

bool DateTime::operator>(const DateTime& other) const noexcept
{
    return compareDatetime(static_cast<struct _datetime*>(impl_), static_cast<struct _datetime*>(other.impl_)) == greater_than;
}

bool DateTime::operator<=(const DateTime& other) const noexcept
{
    return *this < other || *this == other;
}

bool DateTime::operator>=(const DateTime& other) const noexcept
{
    return *this > other || *this == other;
}

bool DateTime::operator==(const DateTime& other) const noexcept
{
    return compareDatetime(static_cast<struct _datetime*>(impl_), static_cast<struct _datetime*>(other.impl_)) == equal_to;
}

DateTime DateTime::operator+(const Duration& other) const noexcept
{
    DateTime updated = *this;
    addTimeDeltaToDateTime(static_cast<struct _datetime*>(updated.impl_), static_cast<struct _timedelta*>(other.impl_),
                           static_cast<struct _datetime*>(updated.impl_));
    return updated;
}

DateTime& DateTime::operator+=(const Duration& other) noexcept
{
    addTimeDeltaToDateTime(static_cast<struct _datetime*>(impl_), static_cast<struct _timedelta*>(other.impl_),
                           static_cast<struct _datetime*>(impl_));
    return *this;
}

Duration DateTime::operator-(const DateTime& other) const noexcept
{
    Duration diff;
    getTimeDeltaFromDateTime(static_cast<struct _datetime*>(impl_), static_cast<struct _datetime*>(other.impl_),
                             static_cast<struct _timedelta*>(diff.impl_));
    return diff;
}

} // namespace yaco::chrono
