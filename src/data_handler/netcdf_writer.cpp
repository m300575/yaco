// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include "netcdf_writer.hpp"

#include "netcdfpp.h"

#include <map>
#include <memory>
#include <yaco/chrono.hpp>
#include <yaco/collection.hpp>
#include <yaco/config.hpp>
#include <yaco/grid.hpp>

namespace yaco
{

std::unique_ptr<DataHandler> DataHandler::createNetCDFWriter(config::Value config)
{
    return std::make_unique<NetCDFWriter>(std::move(config));
}

class NetCDFWriter::Impl {
  public:
    Impl(config::Value config)
            : config_(std::move(config)),
              referenceTime_(config_.at("reference_time").asString())
    {
    }

    void handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end);

  private:
    struct CollectionInfo {
        std::unique_ptr<netCDF::File> file;
        std::size_t timestep_count = 0;
    };
    config::Value config_;
    chrono::DateTime referenceTime_;
    std::map<std::string, CollectionInfo> collectionInfos_;
};

class NetCDFGridVisitor : public GridVisitor {
  private:
    netCDF::File& file_;

  public:
    NetCDFGridVisitor(netCDF::File& file) : file_(file)
    {
    }

    int visit(UnstructuredGrid& grid) override
    {
        if (grid.nbrCells() == 0) {
            throw std::runtime_error("Unstructured grid with no cells");
        }
        file_.add_dimension("ncells", grid.nbrCells());
        int vertices = 0;
        {
            int* nbr_vertices = grid.nbrVerticesPerCell();
            vertices          = nbr_vertices[0];
            for (int i = 0; i < grid.nbrCells(); ++i) {
                if (nbr_vertices[i] != vertices) {
                    throw std::runtime_error("Unstructured grid with cells of different size");
                }
            }
        }
        file_.add_dimension("vertices", vertices);
        {
            auto var = file_.add_variable<float>("clat", { "ncells" });
            var.add_attribute("standard_name").set<std::string>("latitude");
            var.add_attribute("long_name").set<std::string>("center latitude");
            var.add_attribute("units").set<std::string>("radian");
            var.add_attribute("bounds").set<std::string>("clat_bnds");
            var.write<double>(grid.centersLat());
        }
        {
            auto var = file_.add_variable<float>("clon", { "ncells" });
            var.add_attribute("standard_name").set<std::string>("longitude");
            var.add_attribute("long_name").set<std::string>("center longitude");
            var.add_attribute("units").set<std::string>("radian");
            var.add_attribute("bounds").set<std::string>("clon_bnds");
            var.write<double>(grid.centersLon());
        }
        {
            std::vector<double> bnds(grid.nbrCells() * vertices);
            for (int i = 0; i < grid.nbrCells(); ++i) {
                for (int j = 0; j < vertices; ++j) {
                    bnds[i * vertices + j] = grid.verticesLat()[grid.cellToVertex()[i * vertices + j]];
                }
            }
            auto var = file_.add_variable<float>("clat_bnds", std::vector<std::string>{ "ncells", "vertices" });
            var.write<double>(bnds.data());
        }
        {
            std::vector<double> bnds(grid.nbrCells() * vertices);
            for (int i = 0; i < grid.nbrCells(); ++i) {
                for (int j = 0; j < vertices; ++j) {
                    bnds[i * vertices + j] = grid.verticesLon()[grid.cellToVertex()[i * vertices + j]];
                }
            }
            auto var = file_.add_variable<float>("clon_bnds", std::vector<std::string>{ "ncells", "vertices" });
            var.write<double>(bnds.data());
        }
        return 0;
    }
};

NetCDFWriter::NetCDFWriter(config::Value config)
        : impl_(std::make_unique<Impl>(std::move(config)))
{
}

NetCDFWriter::~NetCDFWriter() = default;

static void add_attributes(netCDF::Variable& var, const config::Value& node)
{
    auto metaIter = node.find("meta");
    if (metaIter != node.mapEnd()) {
        auto netcdfIter = metaIter->second.find("netcdf");
        if (netcdfIter != metaIter->second.mapEnd()) {
            for (auto varIter = netcdfIter->second.mapBegin();
                 varIter != netcdfIter->second.mapEnd();
                 ++varIter) {
                var.add_attribute(varIter->first).set<std::string>(varIter->second.asString());
            }
        }
    }
}

void NetCDFWriter::Impl::handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end)
{
    const auto& collectionNode = config_.at("collections").at(collection.name());

    auto& info = collectionInfos_[collection.name()];
    if (!info.file) {
        if (collectionNode.at("collection").asArray().size() != collection.size()) {
            throw std::runtime_error("Collection size mismatch");
        }
        info.file = std::make_unique<netCDF::File>(collectionNode.at("filename").asString(), 'w');
        {
            info.file->add_dimension("time");
            auto var = info.file->add_variable<double>("time", { "time" });
            // TODO support setting the following via config:
            var.add_attribute("standard_name").set<std::string>("time");
            var.add_attribute("calendar").set<std::string>("proleptic_gregorian");
            var.add_attribute("axis").set<std::string>("T");
            var.add_attribute("units").set<std::string>(referenceTime_.toString("seconds since %Y-%m-%D %H:%M:%S"));
        }
        {
            // add grid to netcdf file
            NetCDFGridVisitor visitor(*info.file);
            collection.grid().accept(visitor);
        }

        auto varIter = collectionNode.find("variable");
        if (varIter != collectionNode.mapEnd()) { // the collection elements follow another dimension
            const auto& dim_node  = collectionNode.at("dimension");
            const auto& dimension = dim_node.at("name").asString();
            {
                info.file->add_dimension(dimension, collection.size());
                auto var = info.file->add_variable<float>(dimension, { dimension }); // TODO allow for output type specification
                add_attributes(var, dim_node);
                auto node_it = collectionNode.at("collection").arrayBegin();
                for (std::size_t i = 0; i < collection.size(); ++i) {
                    const auto& node = *node_it;
                    var.set<float, 1>(node.at("dim_value").asFloat(), { i });
                    ++node_it;
                }
            }
            {
                auto var = info.file->add_variable<float>(varIter->second.at("name").asString(), std::vector<std::string>{ "time", dimension, "ncells" });
                var.add_attribute("CDI_grid_type").set<std::string>("unstructured");
                var.add_attribute("number_of_grid_in_reference").set<int>(1);
                var.add_attribute("coordinates").set<std::string>("clat clon");
                add_attributes(var, varIter->second);
            }
        } else { // the collection elements are separate variables
            auto node_it = collectionNode.at("collection").arrayBegin();
            for (std::size_t i = 0; i < collection.size(); ++i) {
                const auto& node     = *node_it;
                const auto& var_node = node.at("variable");
                auto var             = info.file->add_variable<float>(var_node.at("name").asString(), std::vector<std::string>{ "time", "ncells" });
                var.add_attribute("CDI_grid_type").set<std::string>("unstructured");
                var.add_attribute("number_of_grid_in_reference").set<int>(1);
                var.add_attribute("coordinates").set<std::string>("clat clon");
                add_attributes(var, var_node);
            }
        }
    }

    (void)timestep_end;
    info.file->variable("time").require().set<double, 1>(static_cast<double>((timestep_begin - referenceTime_).toSecondsFrom(referenceTime_).count()), { info.timestep_count });
    if (collectionNode.has("variable")) { // the collection elements follow another dimension
        auto var = info.file->variable(collectionNode.at("variable").at("name").asString()).require();
        for (std::size_t i = 0; i < collection.size(); ++i) {
            const auto& field = collection.at(i);
            var.write<double, 3>(field.data(), { info.timestep_count, i, 0 }, { 1, 1, field.size() });
        }
    } else { // the collection elements are separate variables
        auto node_it = collectionNode.at("collection").arrayBegin();
        for (std::size_t i = 0; i < collection.size(); ++i) {
            const auto& node  = *node_it;
            const auto& field = collection.at(i);
            auto var          = info.file->variable(node.at("variable").at("name").asString()).require();
            var.write<double, 2>(field.data(), { info.timestep_count, 0 }, { 1, field.size() });
            ++node_it;
        }
    }
    ++info.timestep_count;
}

void NetCDFWriter::handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end)
{
    impl_->handle(collection, timestep_begin, timestep_end);
}

} // namespace yaco
