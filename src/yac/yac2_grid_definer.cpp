// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include "yac2_grid_definer.hpp"

#include <mpi.h>
#include <numeric>

extern "C" {
#include <yac_config.h>
#include <yac_interface.h>
}

namespace yaco
{

int Yac2GridDefiner::visit(UnstructuredGrid& grid)
{
    int gridId = 0;
    yac_cdef_grid_unstruct(grid.name().c_str(), grid.nbrVertices(), grid.nbrCells(), grid.nbrVerticesPerCell(),
                           grid.verticesLon(), grid.verticesLat(), grid.cellToVertex(), &gridId);

    globalIndex_ = std::vector<int>(static_cast<std::size_t>(grid.nbrCells()), 0);
    std::iota(globalIndex_.begin(), globalIndex_.end(), 0);
    cellCoreMask_ = std::vector<int>(static_cast<std::size_t>(grid.nbrCells()), 1);
    yac_cset_global_index(globalIndex_.data(), YAC_LOCATION_CELL, gridId);
    yac_cset_core_mask(cellCoreMask_.data(), YAC_LOCATION_CELL, gridId);

    int id = 0;
    yac_cdef_points_unstruct(gridId, grid.nbrCells(), YAC_LOCATION_CELL, grid.centersLon(), grid.centersLat(), &id);
    return id;
}

} // namespace yaco
