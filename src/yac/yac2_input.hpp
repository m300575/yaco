// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_YAC_READER_HPP
#define YACO_YAC_READER_HPP

#include <yaco/collection.hpp>
#include <yaco/yac_component.hpp>

namespace yaco
{

class Yac2Input : public YacComponent {
  public:
    Yac2Input(const config::Value& config,
              std::unique_ptr<Grid> grid,
              std::unique_ptr<DataHandler> handler = nullptr);

    ~Yac2Input();
    void run() override;

  private:
    std::string name_;
    std::unique_ptr<Grid> grid_;
    std::unique_ptr<DataHandler> handler_;
    int id_;
    std::vector<Collection> collections_;
    std::string timeStep_;
    int timeLag_;
    std::string startDate_;
    std::string endDate_;
};

} // namespace yaco

#endif // YACO_YAC_READER_HPP
