// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include "healpix_grid.hpp"

#include <algorithm>
#include <healpix_cxx/healpix_base.h>
#include <mpi.h>
#include <numeric>
#include <optional>

extern "C" {
#include <yac_config.h>
#include <yac_interface.h>
}

namespace yaco
{

static void getHealpixGridData(int sides, Grid::HealpixScheme scheme, int& nbrCells, int& nbrVertices, std::vector<int>& nbrVerticesPerCell,
                               std::vector<int>& cellToVertex,
                               std::vector<double>& verticesLon,
                               std::vector<double>& verticesLat,
                               std::vector<double>& centersLon,
                               std::vector<double>& centersLat);

std::unique_ptr<Grid> Grid::createHealpix(std::string name, std::size_t sides, HealpixScheme scheme)
{
    return std::make_unique<HealpixGrid>(name, sides, scheme);
}

HealpixGrid::HealpixGrid(std::string name, std::size_t sides, HealpixScheme scheme)
        : name_(name), sides_(sides), scheme_(scheme)
{
    getHealpixGridData(static_cast<int>(sides), scheme, nbrCells_, nbrVertices_, nbrVerticesPerCell_,
                       cellToVertex_, verticesLon_, verticesLat_,
                       centersLon_, centersLat_);
}

static void getHealpixGridData(int sides, Grid::HealpixScheme scheme, int& nbrCells, int& nbrVertices,
                               std::vector<int>& nbrVerticesPerCell,
                               std::vector<int>& cellToVertex,
                               std::vector<double>& verticesLon,
                               std::vector<double>& verticesLat,
                               std::vector<double>& centersLon,
                               std::vector<double>& centersLat)
{
    constexpr auto vertices_per_cell = 4;

    enum Healpix_Ordering_Scheme healpixScheme;
    switch (scheme) {
        case Grid::HealpixScheme::ring:
            healpixScheme = RING;
            break;
        case Grid::HealpixScheme::nested:
            healpixScheme = NEST;
            break;
        default:
            throw std::runtime_error("Unknown Healpix scheme");
            break;
    }

    std::size_t nbrCells_;
    {
        Healpix_Base hb(sides, healpixScheme);
        nbrCells = hb.Npix();

        nbrCells_ = static_cast<std::size_t>(nbrCells);
        centersLon.resize(nbrCells_);
        centersLat.resize(nbrCells_);

        std::vector<std::tuple<double, double, int>> vertices(nbrCells_ * vertices_per_cell);

        std::vector<vec3> corners;
        for (std::size_t i = 0; i < nbrCells_; ++i) {
            {
                pointing p    = hb.pix2ang(static_cast<int>(i));
                centersLon[i] = p.phi;
                centersLat[i] = (M_PI / 2) - p.theta;
            }

            hb.boundaries(static_cast<int>(i), 1, corners);
            if (corners.size() != 4) {
                throw std::runtime_error("Unexpected number of vertices per cell");
            }

            for (std::size_t j = 0; j < vertices_per_cell; ++j) {
                pointing p;
                p.from_vec3(corners[j]);
                double lon        = p.phi;
                double lat        = (M_PI / 2) - p.theta;
                std::size_t index = i * vertices_per_cell + j;
                vertices[index]   = std::make_tuple(lon, lat, index);
            }
        }

        std::sort(vertices.begin(), vertices.end(), [](const auto& a, const auto& b) {
            return std::get<0>(a) < std::get<0>(b) || (std::get<0>(a) == std::get<0>(b) && std::get<1>(a) < std::get<1>(b));
        });

        cellToVertex.resize(nbrCells_ * vertices_per_cell);
        verticesLon.reserve(nbrCells_ + (1 << sides));
        verticesLat.reserve(nbrCells_ + (1 << sides));

        for (std::size_t i = 0; i < nbrCells_ * vertices_per_cell;) {
            double lon = std::get<0>(vertices[i]);
            double lat = std::get<1>(vertices[i]);
            int index  = static_cast<int>(verticesLon.size());
            verticesLon.push_back(lon);
            verticesLat.push_back(lat);

            while (std::get<0>(vertices[i]) == lon && std::get<1>(vertices[i]) == lat) {
                std::size_t cell_index   = static_cast<std::size_t>(std::get<2>(vertices[i]));
                cellToVertex[cell_index] = index;
                ++i;
            }
        }
    }

    nbrVerticesPerCell.resize(nbrCells_, vertices_per_cell);
    nbrVertices = static_cast<int>(verticesLon.size());
}

std::string HealpixGrid::get_property(const std::string& key) const
{
    if (key == "name") {
        return name_;
    }
    if (key == "nside") {
        return std::to_string(sides_);
    }
    if (key == "order") {
        switch (scheme_) {
            case HealpixScheme::ring:
                return "ring";
            case HealpixScheme::nested:
                return "nested";
        }
    }
    if (key == "sides") {
        return std::to_string(1 << sides_);
    }
    if (key == "type") {
        return "healpix";
    }
    throw std::runtime_error("Unknown healpix grid property '" + key + "'");
}

} // namespace yaco
