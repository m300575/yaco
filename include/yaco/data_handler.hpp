// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_DATA_HANDLER_HPP
#define YACO_DATA_HANDLER_HPP

#include "chrono.hpp"
#include "collection.hpp"
#include "config.hpp"
#include "grid.hpp"

#include <chrono>
#include <memory>
#include <ostream>
#include <utility>

namespace yaco
{

class DataHandler {
  public:
    static std::unique_ptr<DataHandler>
    createDataLogger(bool output_values, std::ostream& os, std::unique_ptr<DataHandler> handler = nullptr);

#ifdef YACO_ENABLE_FDB
    static std::unique_ptr<DataHandler>
    createFDBWriter(config::Value config);
#endif

#ifdef YACO_ENABLE_NETCDF
    static std::unique_ptr<DataHandler>
    createNetCDFWriter(config::Value config);
#endif

    DataHandler()          = default;
    virtual ~DataHandler() = default;

    virtual void handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end) = 0;
};

} // namespace yaco

#endif
