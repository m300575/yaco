// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_FIELD_HPP
#define YACO_FIELD_HPP

#include "grid.hpp"

#include <string>
#include <vector>

namespace yaco
{

using Field = std::vector<double>;

class Collection {
  public:
    Collection(const std::string name, std::size_t size, Grid& grid)
            : name_(name), data_(size), grid_(grid)
    {
        for (auto& field : data_) {
            field.resize(static_cast<std::size_t>(grid.nbrCells()));
        }
    }
    ~Collection() = default;

    const std::string& name() const noexcept { return name_; }

    std::size_t size() const noexcept { return data_.size(); }

    Field& at(std::size_t i) { return data_.at(i); }

    const Field& at(std::size_t i) const { return data_.at(i); }

    const Grid& grid() const noexcept { return grid_; }
    Grid& grid() noexcept { return grid_; }

  private:
    std::string name_;
    std::vector<Field> data_;
    Grid& grid_;
};

} // namespace yaco

#endif
