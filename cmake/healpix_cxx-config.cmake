# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

if(HEALPIX_CXX_INCLUDE_DIR AND HEALPIX_CXX_LIBRARY)
  set(HEALPIX_CXX_FIND_QUIETLY TRUE)
endif()

find_path(HEALPIX_CXX_INCLUDE_DIR healpix_cxx/healpix_base.h
  HINTS
  ENV HEALPIX_CXX_DIR
  ${HEALPIX_CXX_DIR}
  PATH_SUFFIXES include
  )

find_library(HEALPIX_CXX_LIBRARY healpix_cxx
  HINTS
  ENV HEALPIX_CXX_DIR
  ${HEALPIX_CXX_DIR}
  PATH_SUFFIXES lib
  )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(healpix_cxx DEFAULT_MSG HEALPIX_CXX_INCLUDE_DIR HEALPIX_CXX_LIBRARY)

mark_as_advanced(HEALPIX_CXX_INCLUDE_DIR HEALPIX_CXX_LIBRARY)

set(HEALPIX_CXX_LIBRARIES ${HEALPIX_CXX_LIBRARY})
set(HEALPIX_CXX_INCLUDE_DIRS ${HEALPIX_CXX_INCLUDE_DIR})

if(HEALPIX_CXX_FOUND AND NOT TARGET healpix::healpix_cxx)
  add_library(healpix::healpix_cxx UNKNOWN IMPORTED)
  set_target_properties(healpix::healpix_cxx PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${HEALPIX_CXX_INCLUDE_DIRS}")
  set_target_properties(healpix::healpix_cxx PROPERTIES IMPORTED_LINK_INTERFACE_LANGUAGES "CXX")
  set_target_properties(healpix::healpix_cxx PROPERTIES IMPORTED_LOCATION "${HEALPIX_CXX_LIBRARIES}")
  get_filename_component(HEALPIX_CXX_LIBRARIES_EXT "${HEALPIX_CXX_LIBRARIES}" EXT)

  if (NOT HEALPIX_CXX_FIND_QUIETLY)
    message(STATUS "Found HEALPix C++: ${HEALPIX_CXX_LIBRARIES}")
  endif()
endif()
