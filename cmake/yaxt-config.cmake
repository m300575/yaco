# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

include(ExternalProject)

if(NOT TARGET yaxt-proj)
  set(YAXT_PATH ${CMAKE_CURRENT_LIST_DIR}/../external/yaxt)
  enable_language(Fortran)
  find_package(
    MPI
    COMPONENTS C Fortran
    REQUIRED
  )
  externalproject_add(
    yaxt-proj
    EXCLUDE_FROM_ALL TRUE
    SOURCE_DIR ${YAXT_PATH}
    BINARY_DIR ${CMAKE_BINARY_DIR}/external-build/yaxt
    INSTALL_DIR ${CMAKE_BINARY_DIR}/external
    CONFIGURE_COMMAND
      bash -c " \
          ( \
              ${YAXT_PATH}/configure \
              CC=${MPI_C_COMPILER} \
              FC=${MPI_Fortran_COMPILER} \
              --srcdir=${YAXT_PATH} --prefix=<INSTALL_DIR> \
              --disable-shared --enable-static --without-regard-for-quality \
              --with-on-demand-check-programs --without-example-programs --without-perf-programs \
              || ( \
                  cat ${CMAKE_BINARY_DIR}/external-build/yaxt/config.log && exit 1 \
              ) \
      )"
  )
endif()

externalproject_get_property(yaxt-proj INSTALL_DIR)
set(yaxt_INSTALL_DIR ${INSTALL_DIR})

if(NOT TARGET yaxt)
  add_library(yaxt STATIC IMPORTED)
  add_dependencies(yaxt yaxt-proj)
  file(MAKE_DIRECTORY ${yaxt_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_include_directories(yaxt INTERFACE ${yaxt_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_link_libraries(yaxt INTERFACE MPI::MPI_C)
  set_property(
    TARGET yaxt PROPERTY IMPORTED_LOCATION
                         ${yaxt_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}yaxt${CMAKE_STATIC_LIBRARY_SUFFIX}
  )
endif()

if(NOT TARGET yaxt_c)
  add_library(yaxt_c STATIC IMPORTED)
  add_dependencies(yaxt_c yaxt-proj)
  file(MAKE_DIRECTORY ${yaxt_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_include_directories(yaxt_c INTERFACE ${yaxt_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_link_libraries(yaxt_c INTERFACE MPI::MPI_C)
  set_property(
    TARGET yaxt_c PROPERTY IMPORTED_LOCATION
                         ${yaxt_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}yaxt_c${CMAKE_STATIC_LIBRARY_SUFFIX}
  )
endif()

mark_as_advanced(yaxt_DIR)
