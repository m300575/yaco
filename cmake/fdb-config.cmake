# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

if(FDB_INCLUDE_DIR AND FDB_LIBRARY)
  set(FDB_FIND_QUIETLY TRUE)
endif()

find_path(FDB_INCLUDE_DIR NAMES fdb5/api/FDB.h)
find_library(FDB_LIBRARY NAMES fdb5)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(fdb DEFAULT_MSG FDB_INCLUDE_DIR FDB_LIBRARY)

mark_as_advanced(FDB_INCLUDE_DIR FDB_LIBRARY)

set(FDB_LIBRARIES ${FDB_LIBRARY})
set(FDB_INCLUDE_DIRS ${FDB_INCLUDE_DIR})

if(FDB_FOUND AND NOT TARGET fdb)
  add_library(fdb UNKNOWN IMPORTED)
  set_target_properties(fdb PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FDB_INCLUDE_DIRS}")
  set_target_properties(fdb PROPERTIES IMPORTED_LOCATION "${FDB_LIBRARIES}")
  get_filename_component(FDB_LIBRARIES_EXT "${FDB_LIBRARIES}" EXT)
endif()
