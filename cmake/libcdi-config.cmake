# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

include(ExternalProject)

set(LIBCDI_PATH ${CMAKE_CURRENT_LIST_DIR}/../external/libcdi)

find_package(eccodes PATHS ${CMAKE_CURRENT_LIST_DIR} NO_DEFAULT_PATH REQUIRED)
find_package(fdb PATHS ${CMAKE_CURRENT_LIST_DIR} NO_DEFAULT_PATH REQUIRED)

if(NOT TARGET libcdi-proj)
  if(WITH_COVERAGE)
    set(OTHER_LIBS -lgcov)
  else()
    set(OTHER_LIBS "")
  endif()
  externalproject_add(
    libcdi-proj
    EXCLUDE_FROM_ALL TRUE
    SOURCE_DIR ${LIBCDI_PATH}
    BINARY_DIR ${CMAKE_BINARY_DIR}/external-build/libcdi
    INSTALL_DIR ${CMAKE_BINARY_DIR}/external
    DEPENDS eccodes
    UPDATE_DISCONNECTED TRUE
    CONFIGURE_COMMAND
      bash -c " \
          autoreconf -i ${LIBCDI_PATH} \
          && ( \
              ${LIBCDI_PATH}/configure \
              LIBS=\"${ECCODES_LIBRARY} ${FDB_LIBRARY}\" \
              CFLAGS=\"-I${ECCODES_INCLUDE_DIR} -I${FDB_INCLUDE_DIR}\" \
              --srcdir=${LIBCDI_PATH} --with-eccodes=yes --with-fdb5=yes \
              --prefix=<INSTALL_DIR> --disable-shared --enable-static \
              --with-on-demand-check-programs --without-example-programs --disable-cdi-app\
              || ( \
                  cat ${CMAKE_BINARY_DIR}/external-build/libcdi/config.log && exit 1 \
              ) \
          )"
  )
endif()

externalproject_get_property(libcdi-proj INSTALL_DIR)
set(libcdi_INSTALL_DIR ${INSTALL_DIR})

if(NOT TARGET libcdi)
  add_library(libcdi STATIC IMPORTED)
  add_dependencies(libcdi libcdi-proj)
  file(MAKE_DIRECTORY ${libcdi_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_include_directories(libcdi INTERFACE ${libcdi_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_link_libraries(libcdi INTERFACE eccodes fdb5)
  set_property(
    TARGET libcdi PROPERTY IMPORTED_LOCATION
                           ${libcdi_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}cdi${CMAKE_STATIC_LIBRARY_SUFFIX}
  )
endif()

mark_as_advanced(libcdi_DIR)
