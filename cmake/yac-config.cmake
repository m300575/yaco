# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

include(ExternalProject)
include(GNUInstallDirs)

option(EXTERNAL_LAPACK "Use external LAPACK" OFF)

if(NOT TARGET yac-proj)
  set(YAC_PATH ${CMAKE_CURRENT_LIST_DIR}/../external/yac)
  enable_language(Fortran)
  find_package(
    MPI
    COMPONENTS C Fortran
    REQUIRED
  )

  if(EXTERNAL_LAPACK)
    find_package(LAPACK REQUIRED)
    set(LAPACK_CONFIG_VALUE yes)
  else()
    set(LAPACK_CONFIG_VALUE no)
  endif()
  find_package(fyaml REQUIRED PATHS ${CMAKE_CURRENT_LIST_DIR})
  find_package(yaxt PATHS ${CMAKE_CURRENT_LIST_DIR} NO_DEFAULT_PATH REQUIRED)
  get_filename_component(FYAML_LIB_PATH ${FYAML_LIBRARIES} DIRECTORY)
  externalproject_add(
    yac-proj
    EXCLUDE_FROM_ALL TRUE
    SOURCE_DIR ${YAC_PATH}
    BINARY_DIR ${CMAKE_BINARY_DIR}/external-build/yac
    INSTALL_DIR ${CMAKE_BINARY_DIR}/external
    DEPENDS yaxt-proj
    CONFIGURE_COMMAND
      bash -c " \
          ${YAC_PATH}/configure \
          CC=${MPI_C_COMPILER} \
          FC=${MPI_Fortran_COMPILER} \
          --srcdir=${YAC_PATH} --prefix=<INSTALL_DIR> \
          --disable-mpi-checks --disable-netcdf --enable-lib-only --with-yaxt-include=${yaxt_INSTALL_DIR}/include \
          --with-yaxt-lib=${yaxt_INSTALL_DIR}/lib --with-fyaml-include=${FYAML_INCLUDE_DIRS} \
          --with-fyaml-lib=${FYAML_LIB_PATH} --with-external-lapack=${LAPACK_CONFIG_VALUE} \
          || ( \
              cat ${CMAKE_BINARY_DIR}/external-build/yac/config.log && exit 1 \
          )"
  )
endif()

externalproject_get_property(yac-proj INSTALL_DIR)
set(yac_INSTALL_DIR ${INSTALL_DIR})

if(NOT TARGET yac_mtime)
  add_library(yac_mtime STATIC IMPORTED)
  add_dependencies(yac_mtime yac-proj)
  file(MAKE_DIRECTORY ${yac_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_include_directories(yac_mtime INTERFACE ${YAC_PATH}/mtime/include)
  set_property(
    TARGET yac_mtime
    PROPERTY IMPORTED_LOCATION
             ${yac_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}yac_mtime${CMAKE_STATIC_LIBRARY_SUFFIX}
  )
endif()

if(NOT EXTERNAL_LAPACK)
  if(NOT TARGET yac_clapack)
    add_library(yac_clapack STATIC IMPORTED)
    add_dependencies(yac_clapack yac-proj)
    file(MAKE_DIRECTORY ${yac_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
    set_property(
      TARGET yac_clapack
      PROPERTY IMPORTED_LOCATION
               ${yac_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}yac_clapack${CMAKE_STATIC_LIBRARY_SUFFIX}
    )
  endif()
endif()

if(NOT TARGET yac)
  add_library(yac STATIC IMPORTED)
  add_dependencies(yac yac-proj)
  file(MAKE_DIRECTORY ${yac_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_include_directories(yac INTERFACE ${yac_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
  target_link_libraries(yac INTERFACE MPI::MPI_C yac_mtime yaxt_c fyaml::fyaml)
  if(EXTERNAL_LAPACK)
    target_link_libraries(yac INTERFACE LAPACK::LAPACK)
  else()
    target_link_libraries(yac INTERFACE yac_clapack)
  endif()
  set_property(
    TARGET yac PROPERTY IMPORTED_LOCATION
                        ${yac_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}yac${CMAKE_STATIC_LIBRARY_SUFFIX}
  )
endif()

mark_as_advanced(yac_DIR)
