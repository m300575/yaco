# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

if(ECKIT_INCLUDE_DIR AND ECKIT_LIBRARY)
  set(ECKIT_FIND_QUIETLY TRUE)
endif()

find_path(ECKIT_INCLUDE_DIR NAMES eckit/runtime/Main.h)
find_library(ECKIT_LIBRARY NAMES eckit)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(eckit DEFAULT_MSG ECKIT_INCLUDE_DIR ECKIT_LIBRARY)

mark_as_advanced(ECKIT_INCLUDE_DIR ECKIT_LIBRARY)

set(ECKIT_LIBRARIES ${ECKIT_LIBRARY})
set(ECKIT_INCLUDE_DIRS ${ECKIT_INCLUDE_DIR})

if(ECKIT_FOUND AND NOT TARGET eckit)
  add_library(eckit UNKNOWN IMPORTED)
  set_target_properties(eckit PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${ECKIT_INCLUDE_DIRS}")
  set_target_properties(eckit PROPERTIES IMPORTED_LOCATION "${ECKIT_LIBRARIES}")
  get_filename_component(ECKIT_LIBRARIES_EXT "${ECKIT_LIBRARIES}" EXT)
endif()
