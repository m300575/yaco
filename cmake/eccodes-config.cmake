# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

if(ECCODES_INCLUDE_DIR AND ECCODES_LIBRARY)
  set(ECCODES_FIND_QUIETLY TRUE)
endif()

find_path(ECCODES_INCLUDE_DIR NAMES eccodes.h)
find_library(ECCODES_LIBRARY NAMES eccodes)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(eccodes DEFAULT_MSG ECCODES_INCLUDE_DIR ECCODES_LIBRARY)

mark_as_advanced(ECCODES_INCLUDE_DIR ECCODES_LIBRARY)

set(ECCODES_LIBRARIES ${ECCODES_LIBRARY})
set(ECCODES_INCLUDE_DIRS ${ECCODES_INCLUDE_DIR})

if(ECCODES_FOUND AND NOT TARGET eccodes)
  add_library(eccodes UNKNOWN IMPORTED)
  set_target_properties(eccodes PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${ECCODES_INCLUDE_DIRS}")
  set_target_properties(eccodes PROPERTIES IMPORTED_LOCATION "${ECCODES_LIBRARIES}")
  get_filename_component(ECCODES_LIBRARIES_EXT "${ECCODES_LIBRARIES}" EXT)
endif()
